import sbt._
import Keys._

object Dependencies {
  lazy val scalaTest = "org.scalatest" %% "scalatest" % "3.0.1"

  val commonDependencies: Seq[ModuleID] = Seq(
    scalaTest % Test,
    //"org.apache.avro"  %  "avro"  %  "1.8.2",
    "org.imdea.vcd" % "vcdjavaclient" % "0.1",
    "com.google.protobuf" % "protobuf-java" % "3.4.0",
    "org.kohsuke.args4j" % "args4j-maven-plugin" % "2.32",
    "org.apache.zookeeper"% "zookeeper" %"3.4.10",
    "ch.qos.logback" % "logback-classic" % "1.2.3",
    "com.typesafe.scala-logging" %% "scala-logging" % "3.7.2"
  )
  val serverDependencies: Seq[ModuleID] = commonDependencies
  /*
  val serverDependencies: Seq[ModuleID] = commonDependencies ++ Seq(
    "com.sksamuel.avro4s" % "avro4s-core_2.12" % "1.7.0"
  )
  */
  val clientDependencies: Seq[ModuleID] = commonDependencies
}