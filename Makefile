CLIENT_JAR=vcdmapclient/target/vcdmapclient-0.1.0-SNAPSHOT.jar
SERVER_JAR=vcdmapserver/target/vcdmapserver-0.1.0-SNAPSHOT.jar

CLIENT_POM=vcdmapclient/target/vcdmapclient-0.1.0-SNAPSHOT.pom
SERVER_POM=vcdmapserver/target/vcdmapserver-0.1.0-SNAPSHOT.pom

poms:
	sbt makePom

jars:
	sbt package

install:
	mvn install:install-file -Dfile=$(CLIENT_JAR) -DpomFile=$(CLIENT_POM)
	mvn install:install-file -Dfile=$(SERVER_JAR) -DpomFile=$(SERVER_POM)

clean:
	mvn clean
