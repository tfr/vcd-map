import Dependencies._
import Keys._

name := "vcdmap"


lazy val commonSettings = Seq(
  organization := "org.imdea.vcd",
  version := "0.1.0-SNAPSHOT",
  scalaVersion := "2.12.1",

  // disable using the Scala version in output paths and artifacts
  crossPaths := false
)

lazy val vcdmapserver = (project in file("vcdmapserver"))
  .settings(
    commonSettings,
    resolvers += Resolver.mavenLocal,
    libraryDependencies ++= Dependencies.serverDependencies
  )

lazy val vcdmapclient = (project in file("vcdmapclient"))
  .settings(
    commonSettings,
    resolvers += Resolver.mavenLocal,
    libraryDependencies ++= Dependencies.clientDependencies
  ).dependsOn(vcdmapserver)

lazy val root = (project in file("."))
  .aggregate(vcdmapserver, vcdmapclient)
