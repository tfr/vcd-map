package client

import java.io.IOException

import org.scalatest._
import server._

import scala.concurrent._
import ExecutionContext.Implicits.global
import java.util.concurrent.{ExecutorService, Executors}

//TODO:Should try to use the consistency checking tool https://github.com/ssidhanta/ConSpecTool
class ClientSpec extends FlatSpec with Matchers {
  "Simple Check" should "run" in {
    val s1 = new VCDMapServer[Int]("server1", localReads = false, verbose = true, config = Array(""))

    s1.serverInit()

    val send1 = new Thread(new Runnable {
      def run() {
        val c1 = new VCDMapClient(true, s1)
        c1.sendUpdate("a", 3) shouldEqual Left(None)
        c1.sendGet("a") shouldEqual Left(Some(3))
        c1.sendUpdate("b",4) shouldEqual Left(None)
        c1.sendGet("b") shouldEqual Left(Some(4))
      }
    })
    val send2 = new Thread(new Runnable {
      def run(): Unit = {
        val c2 = new VCDMapClient(true, s1)
        c2.sendUpdate("c", 4) shouldEqual Left(None)
        c2.sendGet("c") shouldEqual Left(Some(4))
        c2.sendUpdate("b", 6) shouldEqual Left(None)
        c2.sendGet("b") shouldEqual Left(Some(6))
      }
    })

    send1.start()
    send2.start()
    Thread.sleep(1000)
    println("SERVERMAP:" + s1.mapCopy)
  }
  "Simple Check2" should "run" in {
    val s1 = new VCDMapServer[Int]("server1", localReads = true, verbose = true, config = Array(""))

    s1.serverInit()

    val send1 = new Thread(new Runnable {
      def run() {
        val c1 = new VCDMapClient(true, s1)
        c1.sendUpdate("a", 3) shouldEqual Left(None)
        c1.sendGet("a") shouldEqual Left(Some(3))
        c1.sendUpdate("b",4) shouldEqual Left(None)
        c1.sendGet("b") shouldEqual Left(Some(4))
      }
    })
    val send2 = new Thread(new Runnable {
      def run(): Unit = {
        val c2 = new VCDMapClient(true, s1)
        c2.sendUpdate("c", 4) shouldEqual Left(None)
        c2.sendGet("c") shouldEqual Left(Some(4))
        c2.sendUpdate("b", 6) shouldEqual Left(None)
        c2.sendGet("b") shouldEqual Left(Some(6))
      }
    })

    send1.start()
    send2.start()
    Thread.sleep(1000)
    println("SERVERMAP:" + s1.mapCopy)
  }
}
