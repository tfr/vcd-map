package serialization

import org.scalatest._

class SerializationSpec extends FlatSpec with Matchers {
  "The Serialization object" should "say Map(1->2)" in {
    Serialization.deserialise(Serialization.serialise(Map(1->2))) shouldEqual Map(1->2)
  }

  /*
  "Write Mutable Map to File and read it" should "say Map(1->2)" in {
    var m = scala.collection.mutable.Map[Int, Int](1 -> 2)
    Serialization.writeObjectToFile(m, "/tmp/map.ser")
    var mCopy = Serialization.readObjectFromFile[scala.collection.mutable.Map[Int, Int]]("/tmp/map.ser")
    mCopy shouldEqual m
  }
  */
}
