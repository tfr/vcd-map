/*
import scala.reflect.runtime.{universe => ru}

def f(i: Int) = i + 2

val fn: Any = (f _)
val ref = ru.runtimeMirror(ru.getClass.getClassLoader).reflect(fn)
val apply = ref.symbol.typeSignature.member(ru.TermName("apply"))


def invoke(fun1: Any, arg1: Any): Any = {
  import scala.reflect.runtime.{universe => ru}
  val mirror  = ru.runtimeMirror(ru.getClass.getClassLoader)
  val ref     = mirror.reflect(fn)
  val applies = ref.symbol.typeSignature.member(ru.TermName("apply"))
  val syms    = apply.alternatives.map(_.asMethod)  
  val sym     = syms.find { m =>
    m.paramLists match {
      case (arg :: Nil) :: Nil 
        if arg.asTerm.typeSignature.erasure =:= ru.typeOf[AnyRef] => true
      case _ => false
    }
  } getOrElse sys.error("No generic apply method found")
  ref.reflectMethod(sym)(arg1)
}


// Test:
val fn: Any = (i: Int) => i + 2
invoke(fn, 1)  // res: 3
*/
/*

import scala.reflect.runtime.universe._
def getObjectInstance(clsName: String):AnyRef = {
  val mirror = runtimeMirror(getClass.getClassLoader)
  val module = mirror.staticModule(clsName)
  mirror.reflectModule(module).instance.asInstanceOf[AnyRef]
}
val c:Class[_] = loadIt("Foo", "someJar.jar") // ok 
val o = try c.newInstance.asInstanceOf[AnyRef] catch {
    case i:java.lang.InstantiationException => getObjectInstance("Foo")
}
*/
/*
scala> import scala.reflect.runtime.universe._
import scala.reflect.runtime.universe._

scala> class Foo { def foo(s: String, i: Int) = s * i }
defined class Foo

scala> val params = typeOf[Foo].member(newTermName("foo")).asMethod.paramss.head
params: List[reflect.runtime.universe.Symbol] = List(value s, value i)

scala> params.map(_.typeSignature.typeSymbol)
res0: List[reflect.runtime.universe.Symbol] = List(class String, class Int)
*/
// works
